import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { Container, Flex, Text } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const LocContainer = styled(Container)`
    max-width: 1360px;
`

const Link = styled(NavLink)`
    background-color: #fff;
    color: #377DFF;
    font-weight: bold;
    padding: 20px 0;
    display: block;
    text-align: center;
    max-width: 387px;
    width: 100%;
    border-radius: 6px;
    border: 2px solid #377DFF;
    box-shadow: 0px 3px 6px 0px #D7E5FF;
    transition: .2s linear;

    &:hover {
        background-color: #377DFF;
        color: #fff;
    }
    
    @media(max-width: 1198px){
        margin: 0 auto;
    }
    @media(max-width: 764px){
        padding: 15px 0;
        font-size: 16px;
    }
`

const TextTitle = styled.h2`
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    max-width: 500px;
    @media(max-width: 1198px){
        padding-top: 50px;
        font-size: 28px;
        line-height: 40px;
        text-align: center;
        margin: 0 auto;
    }
    @media(max-width: 764px){
        padding-top: 25px;
        font-size: 22px;
        line-height: 30px;
    }
`

const LocText = styled(Text)`
    line-height: 24px;
    font-size: 16px;
    padding: 30px 0;
    @media(max-width: 1198px){
        text-align: center;
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }
`

const Title = styled.h2`
    max-width: 800px;
    margin: 0 auto;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    text-align: center;
    padding-bottom: 50px;
    @media(max-width: 1198px){
        padding-bottom: 40px;
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        padding-bottom: 25px;
        font-size: 22px;
        line-height: 30px;
    }
`

const TextPart = styled.div`
    max-width: 665px;
    padding-left: 30px;
    @media(max-width: 1198px){
        padding: 0;
    }
`

const ImgPart = styled.div`
    max-width: 526px;
    width: 100%;
    img{
        width: 100%;
    }

    @media(max-width: 1198px){
        max-width: 665px;
    }
`

const LocFlex = styled(Flex)`
    padding: 0 0 240px 0;
    @media(max-width: 1198px){
        flex-direction: column;
        padding: 0 0 180px 0;
    }
    @media(max-width: 764px){
        padding: 0 0 120px 0;
    }
`

const Img = styled.img`
    margin: 0 auto;
    max-width: 1082px;
    width: 100%;
    display: block;
`

const MoreIntegrations = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <LocContainer>
                <LocFlex>
                    <ImgPart>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </ImgPart>
                    <TextPart>
                        <TextTitle>{props.acf.title}</TextTitle>
                        <LocText>{props.acf.text}</LocText>
                        {props.acf.button_type
                            ? <Link to={props.acf.button_link}>{props.acf.button_text}</Link>
                            : <Link as='a' rel="noreferrer" target='_blank' href={props.acf.button_link}>{props.acf.button_text}</Link>
                        }


                    </TextPart>
                </LocFlex>
                <div>
                    <Title>{props.acf.add_title}</Title>
                    <Img alt={props.acf.add_img_alt} src={props.acf.add_img} />
                </div>
            </LocContainer>
        </Article>
    )
}

export default MoreIntegrations