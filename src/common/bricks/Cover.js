import React from 'react'
import styled from 'styled-components'
import { Container, Link, Bounce } from '../styles'

const Article = styled.article`
    margin-top: 150px;
    background-image: linear-gradient(0deg,rgba(0,0,0,0) 0%,rgba(0,0,0,0.35) 75%,rgba(0,0,0,0) 100%), url(${props => props.background});
    background-size: cover;
    background-position: top center;
    background-repeat: no-repeat;
    height: 960px;

    @media(max-width: 1198px) {
       height: 860px;
    }

    @media(max-width: 992px) {
        margin-top: 115px;
    }

    @media(max-width: 764px) {
       height: 640px;
    }
`

const TextWrap = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 100%;
    @media(max-width: 1198px){
    max-width: 680px;
    margin: 0 auto;
}
`

const Title = styled.h1`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 80px;
    color: #FFFFFF;

    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin-bottom: 50px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 40px;
    }
`

const Text = styled.p`
 text-align: center;
color: #FFFFFF;
font-size: 45px;
line-height: 60px;
margin-bottom: 90px;
max-width: 1080px;

@media(max-width: 1198px){
    max-width: 100%;
    margin-bottom: 60px;
    font-size: 28px;
    line-height: 40px;
}

@media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
        margin-bottom: 15px;
    }
`


const LocLink = styled(Link)`
    margin: 0 auto 60px;
    display: block;
    width: 248px;
    box-sizing: border-box;
    text-align: center;
    padding: 15px 0;
    box-shadow: none;
    transition: .2s linear;
    background: #377DFF;
    color: #fff;
    border: 2px solid #377DFF;

    &:hover {
        background: #fff;
        color: #377DFF;
    }

    @media(max-width: 1198px) {
      width: 160px;
    }

`

const Arrow = styled.div`
    width: 0px;
    height: 0px;
    position: relative;
    margin: 40px 0;
    animation: 2.2s infinite ${Bounce};

    &::before,
    &::after{
        content: '';
        position: absolute;
        width: 20px;
        height: 2px;
        background-color: #FFFFFF;
        top: 0;
    }

    &::before{
        transform-origin: left;
        transform: rotateZ(-45deg);
        left: 0;
    }

    &::after{
        transform-origin: right;
        transform: rotateZ(45deg);
        right: 0;
    }
`

const Cover = (props) => {
    return (
        <Article background={props.acf.img}>
            <Container>
                <TextWrap>
                    <Title>{props.acf.title}</Title>
                    <Text>{props.acf.text}</Text>
                    {
                        props.acf.button_type
                            ? <LocLink to={props.acf.button_link}>{props.acf.button_text}</LocLink>
                            : <LocLink as='a' rel="noreferrer" target='_blank' href={props.acf.button_link}>{props.acf.button_text}</LocLink>
                    }
                    <Arrow>
                    </Arrow>
                </TextWrap>
            </Container>
        </Article>
    );
}

export default Cover