import React from 'react'
import styled from 'styled-components'
import { Container } from '../styles'
import Background from '../../sprites/bg_features.png'


const Article = styled.article`
    position: relative;
    margin-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        margin-top:  ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        margin-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 3fr 1fr 3fr;
    grid-gap: 20px;
    @media(max-width: 1480px){
        display: flex;
        flex-direction: column;
        align-items: center;
        grid-gap: 0;
        background-color: #fff;
        max-width: 507px;
        padding: 40px 75px;
        margin: 0 auto;
    }
    @media(max-width: 764px){
        box-sizing: border-box;
        width: 100%;
        max-width: 100%;
        background-color: #377DFF;
        padding: 40px 25px;
    }
`

const LeftCol = styled.div`
    display: flex;
    align-items: center;
`

const TextLeftCol = styled.p`
    font-size: 45px;
    color: #FFFFFF;
    font-weight: bold;
    line-height: 60px;
    @media(max-width: 1480px){
        color: #000000;
        text-align: center;
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        color: #fff;
        font-size: 18px;
        line-height: 24px;
    }
`

const ContentCenter = styled.div`
    width: 360px;
    height: 360px;
    background: #FFFFFF;
    box-shadow: 0 3px 6px #E4E7EA;
    border-radius: 50%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    @media(max-width: 1480px){
        box-shadow: unset;
        flex-direction: row;
        height: auto;
        width: auto;
        padding: 30px 0;
        align-items: center;
    }
    @media(max-width: 764px){
        background-color: #377DFF;
    }
`

const Title = styled.span`
    font-size: 100px;
    font-weight: bold;
    line-height: 80px;
    color: #377DFF;
    margin-bottom: 20px;
    @media(max-width: 1480px){
        margin-bottom: 0px;
        margin-right: 15px;
        font-size: 60px;
        line-height: 80px;
    }
    @media(max-width: 764px){
        color: #fff;
        font-size: 45px;
        line-height: 60px;
    }
`

const Subtitle = styled.span`
    color: #3F4143;
    font-size:60px;
    font-weight: bold; 
    line-height: 80px;
    @media(max-width: 1480px){
        color: #000000;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        color: #fff;
        font-size: 28px;
        line-height: 40px;
    }
`

const RightCol = styled.div`
    display: flex;
    align-items: center;
`

const TextRightCol = styled.p`
    font-size: 45px;
    color: #FFFFFF;
    font-weight: bold;
    line-height: 60px;
    @media(max-width: 1480px){
        color: #000000;
        text-align: center;
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        color: #fff;
        font-size: 18px;
        line-height: 24px;
    }
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    transform: translateY(-50%);
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 305px;
    top: 50%;
    left: 0;
    z-index: 0;
    @media(max-width: 764px){
        display: none
    }
`

const Greater = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Grid>
                    <LeftCol>
                        <TextLeftCol>{props.acf.textleft}</TextLeftCol>
                    </LeftCol>
                    <ContentCenter>
                        <Title>{props.acf.title}</Title>
                        <Subtitle>{props.acf.subtitle}</Subtitle>
                    </ContentCenter>
                    <RightCol>
                        <TextRightCol>
                            {props.acf.textright}
                        </TextRightCol>
                    </RightCol>
                </Grid>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    )
}

export default Greater