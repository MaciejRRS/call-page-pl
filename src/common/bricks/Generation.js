import React from 'react'
import styled from 'styled-components'
import { Container } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    max-width: 1080px;
    margin: 0 auto 105px;

    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`
const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 75px;
    align-items: center;

    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr;
    }

    @media(max-width: 925px){
        grid-template-columns: 1fr;
    }
`

const WrapColumn = styled.div`
    min-height: 225px;
    width: 100%;
    min-height: 320px;
    box-shadow: 0 3px 6px #00000016;
    position: relative;

    img {
        position: absolute;
        left: 15px;
        top: -45px;
    }

    @media(max-width: 1750px){
        /* width: 507px; */
        margin: 0 auto;
    }
    @media(max-width: 1198px){
        /* width: 387px; */
        min-height: 270px;
    }
    @media(max-width: 925px){

        min-height: auto;
    }

    @media(max-width: 764px){
        img {
            width: 90px;
            height: 90px;
        }
    }
`

const ListTitle = styled.h3`
    font-weight: bold;
    padding:30px 15px 0 140px;
    font-size: 28px;
    line-height: 40px;
    margin-bottom: 30px;
    @media(max-width: 1620px){
        margin-bottom: 15px;
        padding: 90px 15px 0 30px;
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        padding: 60px 15px 0 30px;
        font-size: 18px;
        line-height: 28px;
    }
`
const ListText = styled.p`
    padding:0 45px 30px 30px;
    color: #6E7276;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
        padding-bottom: 30px;
    }
`


const Generation = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {
                        props.acf.repeater.map(el =>
                            <WrapColumn>
                                <img alt={el.icon_alt}  src={el.icon} />
                                <ListTitle>{el.title}</ListTitle>
                                <ListText>{el.text}</ListText>
                            </WrapColumn>
                        )
                    }
                </Grid>
            </Container>
        </Article>
    )
}

export default Generation