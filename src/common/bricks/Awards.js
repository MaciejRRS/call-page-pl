import React from 'react'
import styled from 'styled-components'
import { Container } from '../styles'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const List = styled.ul`
    display: flex;
    justify-content: space-evenly;
    align-items: center;

   @media(max-width:1640px) {
    flex-wrap: wrap;
   }
`

const ListItem = styled.div`
padding: 0 15px;
display: flex;
justify-content: center;
@media(max-width: 1640px) {
    margin-bottom: 20px;
}

    :first-child{
        padding-left: 0;
    }

    :last-child{
        padding-right: 0;
    }



@media(max-width: 1640px) {
    flex-basis: 25%;

    :nth-child(3n+1) {
        padding-left: 0;
    }

    :nth-child(3n+3) {
        padding-right: 0;
    }
}
`

const Img = styled.img`
    max-width: 100%; 
`

const Awards = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <List>
                    {
                        props.acf.repeater.map(el =>
                            <ListItem>
                                <Img alt={el.img_alt} src={el.img} />
                            </ListItem>
                        )
                    }
                </List>
            </Container>
        </Article>
    )
}

export default Awards