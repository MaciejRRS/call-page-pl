import React from 'react'
import styled from 'styled-components'
import { Container, Link } from '../styles'


const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const List = styled.div`
    display: flex;
    margin: 0 auto;
    justify-content: space-evenly;
    max-width: 1420px;
    @media(max-width: 1198px){
       flex-direction: column;
    }
    
`

const ListItem = styled.div`
    @media(max-width: 1198px){
       display: flex;
       align-items: center;
       margin: 15px auto;
    }
`

const ListContent = styled.div`
    max-width: 450px;
    box-shadow: 0 3px 6px #E4E7EA;
    border-radius: 12px;
    padding: 115px 30px 30px;
    margin: 0 15px;
    min-height: 475px;
    position: relative;
    height: calc(100% - 380px);
    @media(max-width: 1198px){
        max-width: 510px;
        padding: 30px 30px 30px 90px;
        height: unset;
    }
    @media(max-width: 1198px){
        min-height: auto;
    }
    @media(max-width: 764px){
        padding: 30px;
    }
    
`

const Img = styled.img`
    margin-bottom: -90px;
    margin-left: 30px;
    position: relative;
        z-index: 1;
    @media(max-width: 1198px){
        margin: 0 -90px 0 0;
    }
    @media(max-width: 764px){
        display: none;
    }
`
const ListTitle = styled.h3`
    font-size: 18px;
    font-weight: bold;
    color: #377DFF;
    line-height: 28px;
    text-transform: uppercase;
    margin-bottom: 30px;
`
const ListText = styled.p`
    color: #6E7276;
    font-size: 16px;
    line-height: 24px; 
    margin-bottom: 30px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px; 
    }
`

const ListLink = styled(Link)`
    display: inline-block;
    position: absolute;
    bottom: 30px;
    left: 30px;
    transition: all.2s linear;

&:hover {
    border-color: #377DFF;
    background-color: #FFFFFF;
    color: #000000;
}
    @media(max-width: 1198px){
        left: 90px;
    }
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
    @media(max-width: 764px){
        left: 30px;
    }
`
const SubpageText = styled.h4`
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 30px;
    line-height: 28px;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
`

const ListBlock = styled.ul`
    margin-bottom: 90px;
`

const ListInner = styled.li`
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 20px;
    color: #6E7276;

    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 24px;
    }

    &::before {
        content:'✓';
        font-size: 18px;
        color: #377DFF;
        margin-right: 10px;
    }
`

const Title = styled.h2`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    text-align: center;
    font-weight: bold;
    margin-bottom: 120px;

    @media(max-width: 1198px) {
        max-width: 510px;
        font-size: 45px;
        line-height: 60px;
        margin: 0 auto 80px;
    }

    @media(max-width: 768px) {
        max-width: 100%;
        font-size: 28px;
        line-height: 40px;
        margin: 0 auto 55px;
    }
`

const Clients = (props) => {
    // debugger
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                {props.acf.title
                    ? <Title>{props.acf.title}</Title>
                    : null
                }
                <List>
                    {
                        props.acf.repeater.map(el =>
                            <ListItem>
                                <Img alt={el.img_alt} src={el.img} />
                                <ListContent>
                                    <ListTitle>{el.title}</ListTitle>
                                    <ListText>{el.text}</ListText>
                                    <SubpageText>{el.subtitle}</SubpageText>
                                    <ListBlock>
                                        {
                                            el.repeater.map(innerEl =>

                                                <ListInner>{innerEl.list_item}</ListInner>
                                            )

                                        }
                                    </ListBlock>
                                    {el.button_type
                                        ? <ListLink to={el.button_link}>{el.button_text}</ListLink>
                                        : <ListLink as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</ListLink>
                                    }

                                </ListContent>
                            </ListItem>
                        )
                    }
                </List>
            </Container>
        </Article>
    );
}

export default Clients