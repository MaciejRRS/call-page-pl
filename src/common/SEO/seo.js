import React from 'react'
import { Helmet } from "react-helmet"

const SEO = (props) => {
    return (
        <>
            {
                props.seo
                    ? <Helmet>
                        <title>{props.seo.title}</title>
                        <meta name="description" content={props.seo.description} />

                        <meta property="og:description" content={props.seo.og_description} />
                        <meta property="og:title" content={props.seo.og_title} />
                        <meta property="og:image" content={props.seo.og_image} />
                        <meta property="og:image:width" content={props.seo.og_image_width} />
                        <meta property="og:image:height" content={props.seo.og_image_height} />
                        <meta property="og:url" content={window.location.href} />

                        {props.seo.twitter_description
                            ? <meta name="twitter:description" content={props.seo.twitter_description} />
                            : props.seo.og_description
                                ? <meta name="twitter:description" content={props.seo.og_description} />
                                : <meta name="twitter:description" content={props.seo.description} />
                        }
                        {props.seo.twitter_title
                            ? <meta name="twitter:title" content={props.seo.twitter_title} />
                            : props.seo.og_description
                                ? <meta name="twitter:title" content={props.seo.og_title} />
                                : <meta name="twitter:title" content={props.seo.title} />
                        }
                        {props.seo.image_twitter
                            ? <meta name="twitter:image" content={props.seo.image_twitter} />
                            : props.seo.og_image
                                ? <meta name="twitter:image" content={props.seo.og_image} />
                                : null
                        }

                    </Helmet>
                    : null
            }
        </>
    )
}

export default SEO