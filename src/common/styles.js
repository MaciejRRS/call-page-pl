import { NavLink } from 'react-router-dom'
import styled, { keyframes } from 'styled-components'

export const Container = styled.div`
    margin: 0 auto;
    height: 100%;
    position: relative;
    z-index: 1;
    max-width: 1640px;
    width: calc(100% - 120px);
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }
`

export const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`

export const Text = styled.p`
    font-weight: normal;
    color: #6E7276;
    line-height: 28px;
`

export const Link = styled(NavLink)`
    padding: 15px 60px;
    font-weight: bold;
    background-color: #377DFF;
    color: #ffffff;
    border: none;
    border-radius: 6px;
    border: 2px solid #377DFF;
    box-shadow: 0px 3px 6px 0px #D7E5FF;
`

export const Button = styled.button`
    padding: 15px 60px;
    color: #ffffff;
    font-weight: bold;
    background-color: #377DFF;
    border: none;
    border-radius: 6px;
    border: 2px solid #377DFF;
    box-shadow: 0px 3px 6px 0px #D7E5FF;
    cursor: pointer;
`

export const Input = styled.input`
    border: 2px solid #A7C6FF;
    border-radius: 6px;
    padding: 15px 30px;
    background-color: #fff;
    box-shadow: 0px 3px 6px 0px #D7E5FF;

    &::placeholder{
        color: #C2C9D1;
    }
`

export const Bounce = keyframes`
0% { 
    transform: translateY(0);
  }
  50%   { 
    transform: translateY(20px);
  }

  100%   { 
    transform: translateY(0);
  }
`;

