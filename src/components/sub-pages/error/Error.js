import React from 'react'
import styled from 'styled-components'
import { connect } from "react-redux"
import { Container, Flex, Link } from '../../../common/styles'
import SEO from '../../../common/SEO/seo'

const LocFlex = styled(Flex)`
    justify-content: space-evenly;

    @media(max-width: 996px){
        justify-content: center;
        padding-top: 120px;
        img{
            display: none;
        }
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    @media(max-width: 996px){
        text-align: center;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 470px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    font-size: 28px;
    line-height: 40px;
    padding-top: 20px;
    padding-bottom: 80px;
    color: #6E7276;
    max-width: 387px;
    @media(max-width: 996px){
        text-align: center;
        font-size: 22px;
        line-height: 30px;
        padding-bottom: 60px;
    }
    @media(max-width: 470px){
        font-size: 18px;
        line-height: 24px;
        padding-bottom: 40px;
    }
`

const LocLink = styled(Link)`
    @media(max-width: 996px){
        display: block;
        text-align: center;
    }
    @media(max-width: 470px){
        font-size: 16px;
    }
`

const Error = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Container>
                <LocFlex>
                    <div>
                        <Title>{props.acf.title}</Title>
                        <Text>{props.acf.text}</Text>
                        <LocLink to={props.acf.button_link}>{props.acf.button_text}</LocLink>
                    </div>
                    <div>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </div>
                </LocFlex>
            </Container>
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[24].acf
    }
}

export default connect(mapStateToProps, {})(Error)