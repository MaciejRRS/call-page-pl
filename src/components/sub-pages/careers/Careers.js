import React from 'react'
import { connect } from "react-redux";
import Cover from '../../../common/bricks/Cover'
import Grid from './components/Grid'
import Awards from '../../../common/bricks/Awards'
import Values from './components/Values'
import Branchers from './components/Branchers'
import Hiring from './components/Hiring'
import SEO from '../../../common/SEO/seo'



const Careers = (props) => {

    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Cover acf={props.acf.cover} />
            <Awards marginDesktop='180px' marginTablet='120px' marginPhone='65px' acf={props.acf.awards} />
            <Grid acf={props.acf.grid} />
            <Values acf={props.acf.Values} />
            <Branchers acf={props.acf.branchers} />
            <Hiring acf={props.acf.hiring} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[25].acf // 
    }
}

export default connect(mapStateToProps, {})(Careers)