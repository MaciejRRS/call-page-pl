import React from 'react'
import Hero from './components/Hero'
import Params from './components/Params'
import { connect } from "react-redux"
import SEO from '../../../common/SEO/seo'

const Features = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} />
            <Params acf={props.acf.params} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[4].acf // Features
    }
}

export default connect(mapStateToProps, {})(Features)