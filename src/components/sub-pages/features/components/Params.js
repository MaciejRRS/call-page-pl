import React, { useState } from 'react'
import styled from 'styled-components'
import { Container, Text } from './../../../../common/styles'

const Nav = styled.nav`
    width: 100%;
    position: sticky;
    margin-top: 100px;
    top: 100px;
    z-index: 9;
    background-color: #fff;
    box-shadow: 0 3px 6px 0 #00000016;
    padding: 20px 0;
`

const NavList = styled.ul`
    display: flex;
    justify-content: space-between;
    align-items: center;
    @media(max-width: 1198px){
        flex-direction: column;
        transition: .3s linear;
        height: ${props => props.isOpened ? 'auto' : '0px'};
        opacity: ${props => props.isOpened ? '1' : '0'};
        overflow: hidden;
        pointer-events: ${props => props.isOpened ? 'all' : 'none'};

        li{
            padding: 10px 0;
            a{
                font-size: 16px;
            }
        }
    }
`

const NavLink = styled.a`
    color: #000;
    transition: .2s linear;
    cursor: pointer;
    position: relative;
    overflow: hidden;

    img {
        transition: .1s linear;
        position: absolute;
        bottom: -5px;
        left: 0;
        width: 100%;
    }
    &:hover {
    color: ${props => props.hoverColor};
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 80px;
    grid-row-gap: 60px;
    @media(max-width: 1350px){
        grid-template-columns: 1fr 1fr 1fr;
    }
    @media(max-width: 1080px){
        grid-template-columns: 1fr 1fr;
        grid-column-gap: 45px;
        grid-row-gap: 30px;
    }
    @media(max-width: 564px){
        grid-template-columns: 1fr;
    }
`

const Title = styled.h2`
    font-size: 45px;
    margin: 150px 0 90px;
    font-weight: bold;
    text-align: center;
    @media(max-width: 1080px){
        font-size: 28px;
        line-height: 40px;
        margin: 100px 0 60px;
    }
    @media(max-width: 564px){
        grid-template-columns: 1fr;
        margin: 60px 0 30px;
        font-size: 18px;
        line-height: 28px;
    }
`

const Flex = styled.div`
    display: flex;
    margin-bottom: 10px;
    align-items: center;
`

const Img = styled.img`
    margin-right: 15px;
`

const ItemTitle = styled.h3`
    @media(max-width: 1080px){
        font-size: 16px;
        line-height: 24px;
    }
`

const ItemText = styled(Text)`
    @media(max-width: 1080px){
        font-size: 14px;
        line-height: 20px;
    }
`
const Anchor = styled.div`  
    padding-top: 120px;
    margin-top: -120px;
`

const ListMenu = styled.h3`
    display: none;
    @media(max-width: 1198px){
        width: 100%;
        display: block;
        text-align: center;
    }
`

const LineColor = styled.img`
    display: block;
`

const Params = (props) => {

    const [isMenuOpened, isMenuOpenedChange] = useState(false)

    const OpenMenu = () => {
        isMenuOpenedChange(!isMenuOpened)
    }
    return (
        <React.Fragment>
            <Nav>
                <Container>
                    <ListMenu onClick={() => { OpenMenu() }}>Filter</ListMenu>
                    <NavList isOpened={isMenuOpened}>
                        {
                            props.acf.params_repeater.map(el =>
                                <React.Fragment>
                                    <li>
                                        <NavLink hoverColor={el.underline_color_hover} onClick={() => { OpenMenu() }} href={'#' + el.title}>{el.title}
                                            <LineColor alt='underline' src={el.underline} /></NavLink>
                                    </li>
                                </React.Fragment>
                            )
                        }
                    </NavList>
                </Container>
            </Nav>
            <article>
                <Container>
                    {props.acf.params_repeater.map(el =>
                        <Anchor id={el.title}>
                            <Title>{el.title}</Title>
                            <Grid>
                                {el.inner_items_repeater.map(innerEl =>
                                    <div>
                                        <Flex>
                                            <Img alt={innerEl.icon_alt} src={innerEl.icon} />
                                            <ItemTitle>{innerEl.title}</ItemTitle>
                                        </Flex>
                                        <div>
                                            <ItemText>{innerEl.text}</ItemText>
                                        </div>
                                    </div>
                                )}
                            </Grid>
                        </Anchor>
                    )}
                </Container>
            </article>
        </React.Fragment>
    )
}

export default Params