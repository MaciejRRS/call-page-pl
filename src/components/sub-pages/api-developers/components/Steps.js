import React from 'react'
import styled from 'styled-components'
import { Container, Link } from '../../../../common/styles'


const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '90px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '60px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const List = styled.ul`
    position: relative;
    :after {
        content:'';
        width: 2px;
        height: 100%;
        background: red;
        position: absolute;
        top: 0;
        left: 50%;
        background: #A7C6FF;
    }

    li:nth-child(odd){
        flex-direction: row;

        @media(max-width: 1048px){
            flex-direction: column;
        }
    }

    @media(max-width: 1048px){
        &:after{
            display: none;
        }
    }


`

const ListItem = styled.li`
    display: flex;
    justify-content: space-between;
    flex-direction: row-reverse;
    align-items: center;
    margin-top: 150px;
    position: relative;

    :before {
        content:'';
        width: 30px;
        height: 30px;
        background: red;
        position: absolute;
        top: 0;
        left: calc(50% - 15px);
        background: #A7C6FF;
        border-radius: 50%;
    }

    :last-child {
       padding-bottom: 60px;
    }

    @media(max-width: 1048px){
        flex-direction: column;
        margin-top: 0;

        &:before{
            display: none;
        }
    }
`

const Img = styled.img`
    width: 100%;
    height: 320px;
`

const ImgWrapper = styled.div`
    width: 526px;
    @media(max-width: 1340px){
        width: 45%;
    }
    @media(max-width: 1048px){
        width: 100%;
        img{
            display: block;
            margin: 0 auto;
        }
    }
`

const TextWrapper = styled.div`
    width: 526px;
    @media(max-width: 1340px){
        width: 45%;
    }
    @media(max-width: 1048px){
        width: 100%;
        max-width: 665px;
        margin: 30px 0 45px;
    }
`

const ListTitle = styled.h2`
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){

    }
`

const ListSubTitle = styled.p`
    padding: 20px 0 30px;
    font-size: 28px;
    line-height: 40px;
    color: #6E7276;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;

    }
    @media(max-width: 1048px){
        margin-bottom: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        
    }
`

const Steps = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <List>
                    {
                        props.acf.repeater.map(el =>
                            <ListItem>
                                <ImgWrapper>
                                    <Img alt={el.img_alt} src={el.img} />
                                </ImgWrapper>
                                <TextWrapper>
                                    <ListTitle>{el.title}</ListTitle>
                                    <ListSubTitle>{el.text}</ListSubTitle>
                                    {el.button_type
                                        ? <Link to={el.button_link}>{el.button_text}</Link>
                                        : <Link as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</Link>
                                    }
                                </TextWrapper>
                            </ListItem>
                        )
                    }
                </List>
            </Container>
        </Article>
    )
}

export default Steps