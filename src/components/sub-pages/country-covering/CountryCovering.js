import React from 'react'
import { connect } from "react-redux"
import Content from './components/Content'
import SEO from '../../../common/SEO/seo'


const CountryCovering = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Content acf={props.acf.content} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[26].acf
    }
}

export default connect(mapStateToProps, {})(CountryCovering)