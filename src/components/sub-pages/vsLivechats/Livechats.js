import React from 'react'
import { connect } from "react-redux"
import CustomerMotivation from '../../../common/bricks/CustomerMotivation'
import Develope from '../../../common/bricks/Develope'
import Awards from '../../../common/bricks/Awards'
import Dominating from '../../../common/bricks/Dominating'
import Features from '../../../common/bricks/Features'
import TrustedBy from '../../../common/bricks/TrustedBy'
import Generation from '../../../common/bricks/Generation'
import Mixes from '../../../common/bricks/Mixes'
import Questions from '../../../common/bricks/Questions'
import VoiceChat from '../../../common/bricks//VoiceChat'
import SEO from '../../../common/SEO/seo'

const Livechats = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <TrustedBy acf={props.acf.hero} />
            <Generation acf={props.acf.generation} />
            <VoiceChat acf={props.acf.voice_chat} />
            <CustomerMotivation acf={props.acf.customer_motivation} />
            <Dominating acf={props.acf.dominating_channel} />
            <Mixes acf={props.acf.mix_of_chat} />
            <Features acf={props.acf.features} />
            <Develope acf={props.acf.develope} />
            <Awards marginDesktop='240px' marginTablet='260px' marginPhone='280px' acf={props.acf.awards} />
            <Questions acf={props.acf.questions} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[10].acf // vsLivechats
    }
}

export default connect(mapStateToProps, {})(Livechats)