import React from 'react'
import styled from 'styled-components'
import { Container, Link } from '../../../../common/styles'



const Article = styled.article`

`

const Title = styled.h2`
    font-size: clamp(28px, 3.5vw, 45px);
    line-height: clamp(40px, 3.5vw, 60px);
    font-weight: bold;
    max-width: 1080px;
    text-align: center;
    padding-top: 240px;
    margin: -240px auto 120px;

    @media(max-width: 1198px) {
        padding-top: 240px;
        margin: -240px auto 55px;
        font-size: 28px;
        line-height: 40px;
    }

    @media(max-width: 764px) {
        padding-top: 180px;
        margin: -180px auto 60px;
    }
`



const List = styled.ul`


    li:nth-child(odd){
        flex-direction: row-reverse;

        @media(max-width: 1199px) {
            flex-direction: column;
        }

        img {
            margin-left: 15px;
    width: auto;
    height: 400px;
            @media(max-width: 1199px) {
           margin-left: 0;
           max-width: 100%;
           height: auto;
           max-height: 400px;
        }
        }
    }

    li:nth-child(even){
      

        img {
            margin-right: 15px;
            width: auto;
            height: 400px;

        @media(max-width: 1199px) {
           margin-right: 0;
           max-width: 100%;
           height: auto;
           max-height: 400px;
        }
            
        }
    }

`

const ListItem = styled.li`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    margin-top: 120px;
    position: relative;
    :first-child {
                margin-top: 0;
            }
    @media(max-width: 1199px) {
            flex-direction: column;
            max-width: 65vw;
            margin: 60px auto 0;
        }

        @media (max-width:767px) {
            margin-top: 45px;
            max-width: 100%;
        }
   
`

const ImgContener = styled.div`
width: 50%;
display: flex;
justify-content: center;

    @media(max-width: 1199px) {
            width: 100%;
            margin-bottom: 30px;
        }
`

const Img = styled.img`
    max-width: 100%;
    width: 100%;
   
`

const TextWrapper = styled.div`
    width: 665px;
    @media(max-width: 1199px) {
            width: 100%;
        }
`

const ListTitle = styled.h3`
    font-size: 28px;
    font-weight: bold;
    line-height: 40px;
    @media (max-width:1199px) {
        font-size: 18px;
        line-height: 28px;
        margin-bottom: 15px; 
        max-width: 420px;
    }
    @media (max-width:767px) {
        font-size: 16px;
        line-height: 24px;
        max-width: 100%;
    }
`

const ListSubTitle = styled.p`
    padding: 30px 0 40px;
    font-size: 16px;
    color: #6E7276;
    line-height: 24px;
    @media (max-width:1199px) {
        font-size: 14px;
        padding: 15px 0 40px;
      }
    @media (max-width:764px) {
        line-height: 20px;
        padding: 12px 0 10px;
    }
    `


const ListLink = styled(Link)`
    display: block;
    padding: 20px 0;
    text-align: center;
    width: 248px;
    color: #000000;
    background-color: #FFFFFF;
    border: 2px solid #377DFF;
    transition: all.2s linear;

    @media(max-width:764px) {
        padding: 10px 0;
        margin: 0 auto;
        font-size: 16px;
    }

&:hover {
    border-color: #377DFF;
    background-color: #377DFF;
    color: #FFFFFF;
}
`



const Repeater = (props) => {
    return (
        <Article>
            <Container>
                <List>
                    {
                        props.acf.repeater.map(el =>
                            <>
                                <ListItem>
                                    <Title id={el.id}>{el.title}</Title>
                                </ListItem>
                                <List>
                                    {
                                        el.repeater.map(innerEl =>
                                            <ListItem>
                                                <ImgContener>
                                                    <Img alt={innerEl.img_alt} src={innerEl.img} />
                                                </ImgContener>
                                                <TextWrapper>
                                                    <ListTitle>{innerEl.title}</ListTitle>
                                                    <ListSubTitle>{innerEl.text}</ListSubTitle>
                                                    {innerEl.button_text ? <ListLink to={innerEl.button_link}>{innerEl.button_text}</ListLink> : null}
                                                </TextWrapper>
                                            </ListItem>
                                        )
                                    }
                                </List>
                            </>
                        )
                    }



                </List>
            </Container>







        </Article>
    )
}

export default Repeater