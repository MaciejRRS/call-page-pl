import React from 'react'
import styled from 'styled-components'
import { Container } from '../../../../common/styles'



const Article = styled.article`
    padding-top: 90px;

    @media (max-width: 1198px) {
        padding-top: 120px;
    }
    @media (max-width: 764px) {
        padding-top: 90px;
    }
`

const Title = styled.h2`
    font-size: clamp(22px, 3.5vw, 28px);
    line-height: clamp(24px, 3.5vw, 40px);
    font-weight: bold;
    max-width: 1080px;
    text-align: center;
    margin: 0 auto 30px;

        @media (max-width: 1198px) {
            font-size: 28px;
            line-height: 40px;
            margin: 0 auto 25px;
        }

        @media (max-width: 764px) {
            font-size: 22px;
            line-height: 30px;
        }
`

const Text = styled.p`
    font-size: 16px;
    line-height: 24px;
    max-width: 1080px;
    text-align: center;
    margin: 0 auto 60px;
    color: #6E7276;
    @media (max-width: 1198px) {
        font-size: 16px;
     }

     @media (max-width: 764px) {
        font-size: 14px;
        line-height: 20px;
        margin-bottom: 45px;
     }
`

const List = styled.ul`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    margin-bottom: 170px;
    @media (max-width:1198px) {
        grid-template-columns: 1fr 1fr 1fr;
        grid-column-gap: 15px;
        grid-row-gap: 15px;
        margin-bottom: 90px;
    }
    @media (max-width:764px) {
        grid-template-columns: 1fr 1fr;
        grid-column-gap: 10px;
        grid-row-gap: 10px;
        margin-bottom: 85px;
    }
`

const ListItem = styled.li`

`

const Img = styled.img`
width: 100%;
max-width: 100%;
`






const System = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <Text>{props.acf.text}</Text>
                <List>
                    {
                        props.acf.repeater.map(el =>
                            <ListItem>
                                <Img alt={el.img_alt} src={el.img} />
                            </ListItem>
                        )
                    }
                </List>
            </Container>
        </Article>
    )
}

export default System