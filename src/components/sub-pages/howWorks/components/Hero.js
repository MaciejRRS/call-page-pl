import React from 'react'
import styled from 'styled-components'
import { Container, Bounce } from '../../../../common/styles'

const Article = styled.article`
    position: relative;
    height: 100%;
    padding-top: 180px;
    @media(max-width: 1199px){
        padding-top: 60px;
    }
`
const Title = styled.h1`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 48px;

    @media(max-width: 1199px) {
        font-size: 45px;
        margin-bottom: 20px;
        line-height: 60px;
        text-align: left;
    }

    @media(max-width: 767px) {
        font-size: 28px;
        margin-bottom: 10px;
        line-height: 40px;
    }
`

const Subtitle = styled.p`
    text-align: center;
    color: #6E7276;
    font-size: 28px;
    max-width: 1080px;
    margin: 0 auto 110px;
    line-height:40px;

    @media(max-width: 1199px) {
        font-size: 18px;
        margin-bottom: 90px;
        line-height: 28px;
        text-align: left;
    }

    @media(max-width: 767px) {
        font-size: 16px;
        margin-bottom: 60px;
        line-height: 24px;
    }
`


const Arrow = styled.div`
    width: 0px;
    height: 0px;
    position: relative;
    left: 50%;
    margin: 40px 0;
    animation: 2.2s infinite ${Bounce};

    &::before,
    &::after{
        content: '';
        position: absolute;
        width: 20px;
        height: 1px;
        background-color: #377DFF;
        top: 0;
    }

    &::before{
        transform-origin: left;
        transform: rotateZ(-45deg);
        left: 0;
    }

    &::after{
        transform-origin: right;
        transform: rotateZ(45deg);
        right: 0;
    }
    @media(max-width: 1199px) {
        display: none;
    }
`

const Nav = styled.nav`
    width: 100%;
    position: sticky;
    /* margin-top: 100px; */
    top: 0;
    left: 0;
    right: 0;
    z-index: 98;
    background-color: #fff;
    box-shadow: 0 3px 6px 0 #00000016;
    padding: 20px 0;
    margin-bottom: 120px;

    @media(max-width: 1198px) {
        position: relative;
        margin-bottom: 0;
    }
`

const NavList = styled.ul`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    li:nth-child(1), li:nth-child(2), li:nth-child(3){
        font-weight: bold;
    }


    @media(max-width: 1300px){
        justify-content: center;
    }

    @media(max-width: 1198px){
        li{
            padding: 10px 0;
        }
    }

    @media(max-width: 764px){
        flex-direction: column;
    }
`


const NavLinkWrap = styled.div`
position: relative;
margin: 0 5px;

img {
    transition: .1s linear;
    position: absolute;
    bottom: -5px;
    left: 0;
    width: 100%;
}
`
const NavLinkLoc = styled.a`
        font-size: 18px;
        color: #000000;
        transition: all.2s linear;
            @media(max-width: 1300px){
                padding: 20px 0;
                font-size: 16px;
        }

        &:hover {
            color: ${props => props.hoverColor};
        }

`
const LineColor = styled.img`
    width: 100%;
`

const Hero = (props) => {
    return (
        <>
            <Article>
                <Container>
                    <Title>{props.acf.title}</Title>
                    <Subtitle>{props.acf.text}</Subtitle>
                </Container>
            </Article>
            <Nav>
                <Container>
                    <NavList>
                        {props.anchor.map(el =>
                            <li>
                                <NavLinkWrap>
                                    <NavLinkLoc hoverColor={el.underline_color_hover} href={'#' + el.anhor_url}>{el.anhor}</NavLinkLoc>
                                    <LineColor src={el.underline} />
                                </NavLinkWrap>
                            </li>
                        )}
                    </NavList>
                </Container>
            </Nav>
            <Arrow>
            </Arrow>
        </>
    )
}

export default Hero