import React from 'react'
import { connect } from "react-redux"
import Develope from '../../../common/bricks/Develope'
import Clients from '../../../common/bricks/Clients'
import Features from '../../../common/bricks/Features'
import Generation from '../../../common/bricks/Generation'
import TrustedBy from '../../../common/bricks/TrustedBy'
import ProblemsResolving from '../../../common/bricks/ProblemsResolving'
import Questions from '../../../common/bricks/Questions'
import Awards from '../../../common/bricks/Awards'
import CaptureLeads from '../../../common/bricks/CaptureLeads'
import SEO from '../../../common/SEO/seo'

const Popups = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <TrustedBy acf={props.acf.hero} />
            <CaptureLeads acf={props.acf.capture_leads} />
            <ProblemsResolving acf={props.acf.problem_resolving} />
            <Clients acf={props.acf.clients} />
            <Generation acf={props.acf.generation} />
            <Features acf={props.acf.features} />
            <Develope acf={props.acf.develope} />
            <Awards marginDesktop='240px' marginTablet='260px' marginPhone='280px' acf={props.acf.awards} />
            <Questions acf={props.acf.questions} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[12].acf // vsLivechats
    }
}

export default connect(mapStateToProps, {})(Popups)