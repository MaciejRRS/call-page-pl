import React from 'react'
import { connect } from "react-redux"
import Price from './components/Price'
import MoreEffective from '../../../common/bricks/MoreEffective'
import Questions from '../../../common/bricks/Questions'
import MoreThen from './components/MoreThen'
import Countries from './components/Countries'
import Solution from './components/Solution'
import Table from './components/Table'
import SEO from '../../../common/SEO/seo'

const Pricing = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <MoreEffective marginDesktop='60px' marginTablet='60px' marginPhone='60px'  acf={props.acf.hero} />
            <Price acf={props.acf.price} />
            <MoreThen acf={props.acf.more_then} />
            <Countries acf={props.acf.countries} />
            <Solution acf={props.acf.solution} />
            <Table acf={props.acf.table} />
            <Questions acf={props.acf.questions} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[28].acf
    }
}

export default connect(mapStateToProps, {})(Pricing)