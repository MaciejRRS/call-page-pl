import React from 'react'
import { Container, Text } from '../../../../common/styles'
import ContactForm from '../../../../common/form/Form';
import styled from 'styled-components'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    @media(max-width: 1198px){
        
    }
`

const LocText = styled(Text)`
    text-align: center;
    font-size: 28px;
    line-height: 40px;
    padding: 50px 0 120px;
`


const Form = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <LocText>{props.acf.sub_title}</LocText>
                <ContactForm portalId={props.acf.portal_id} formId={props.acf.form_id} />
            </Container>
        </Article>
    )
}

export default Form