import React from 'react'
import styled from 'styled-components'
import MiniForm from './../../../../common/mini-form/Form'

const Container = styled.div`
    max-width: 1360px;
    margin: 0 auto;
     width: calc(100% - 120px);
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }
`

const Title = styled.h2`
    font-size: 45px;
    line-height: 60px;
    padding: 0 0 120px;
    text-align: center;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding-bottom: 90px;
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
    @media(max-width: 564px){
        font-size: 22px;
        line-height: 30px;
        padding-bottom: 60px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 15px;

    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr 1fr;
        grid-column-gap: 15px;
    }
    @media(max-width: 764px){
        grid-template-columns: 1fr 1fr;
    }
`

const Item = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 20px 50px;
    img {
        max-width: 100%;
        width: 180px;
        max-height: 120px;
        width:auto;
    }
    @media(max-width: 1096px){
        padding: 20px;
    }
    @media(max-width: 764px){
        padding: 20px 40px;
    }
    @media(max-width: 496px){
        padding: 10px;
    }
`
const AreaMiniForm = styled.div`


@media(max-width: 1198px) {
        
    }

    @media(max-width: 764px) {
       
    }
`


const Hero = (props) => {
    return (
        <article>
            <Container>
                <AreaMiniForm>
                    <MiniForm acf={props.acf.form} />
                </AreaMiniForm>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {props.acf.repeater.map(el =>
                        <Item>
                            <img alt={el.img_alt} src={el.img} />
                        </Item>
                    )}
                </Grid>


            </Container>
        </article>
    )
}

export default Hero