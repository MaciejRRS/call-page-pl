import React from 'react'
import { Container } from '../../../../common/styles'
import styled from 'styled-components'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-gap: 30px;

    @media(max-width: 996px){
        grid-template-columns: 1fr 1fr 1fr;
    }

    @media(max-width: 764px){
        grid-template-columns: 1fr 1fr;
    }
`

const Img = styled.img`
    margin-bottom: 30px;
    @media(max-width: 1198px){
        max-width: 120px;
        display: block;
        margin: 0 auto 30px;
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const SubTitle = styled.h3`
    text-align: center; 
    padding: 50px 0 60px;
    max-width: 1082px; 
    margin: 0 auto;
    color: #6E7276;
    font-weight: normal;
    font-size: 28px;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        padding: 40px 0;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
        padding: 30px 0;
    }
`

const ItemTitle = styled.p`
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    line-height: 24px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px;
    }
`

const ItemText = styled.p`
    text-align: center;
    font-size: 16px;
    line-height: 24px;
    color: #6E7276;
    padding-top: 10px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px;
    }
`

const Item = styled.div`
    margin: 0 auto;
`

const Team = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <SubTitle>{props.acf.text}</SubTitle>
                <Grid>
                    {
                        props.acf.repeater.map(el =>
                            <Item>
                                <Img alt={el.img_alt} src={el.img} />
                                <ItemTitle>{el.title}</ItemTitle>
                                <ItemText>{el.text}</ItemText>
                            </Item>
                        )
                    }
                </Grid>
            </Container>
        </Article>
    )
}

export default Team