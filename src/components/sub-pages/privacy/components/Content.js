import React from 'react'
import { Container } from '../../../../common/styles'
import styled from 'styled-components'

const LocContainer = styled(Container)`

margin-top: 140px;
@media(max-width: 764px) {
    margin-top: 90px;
}
    p{
        padding-bottom: 25px;
        color: #6E7276;
    }
    h1,h2{
        font-size: 45px;
        line-height: 60px;
        font-weight: bold;
        padding: 25px 0 50px;
    @media(max-width: 1198px){
        padding: 30px 0 30px;
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }


    & strong {
        font-size: 45px;
        line-height: 60px;
        font-weight: bold;
        padding: 0 0 50px;

        @media(max-width: 1198px){
            padding: 30px 0 30px;
            font-size: 28px;
            line-height: 40px;
        }
        @media(max-width: 764px){
            font-size: 22px;
            line-height: 30px;
        }
    }
}

h3{
    font-size: 28px;
    line-height: 45px;
    font-weight: bold;
    padding: 0 0 35px;
    @media(max-width: 1198px){
        padding: 30px 0 30px;
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }


    & strong {
        font-size: 28px;
        line-height: 45px;
        font-weight: bold;
        padding: 0 0 35px;

        @media(max-width: 1198px){
            padding: 60px 0 30px;
            font-size: 28px;
            line-height: 40px;
        }
        @media(max-width: 764px){
            font-size: 18px;
            line-height: 30px;
        }
    }
}

h4{
    font-size: 20px;
    line-height: 25px;
    font-weight: bold;
    padding: 0 0 25px;
    @media(max-width: 1198px){
        padding: 30px 0 30px;
        font-size: 18px;
    }
    @media(max-width: 764px){
        font-size: 16px;
    }


    & strong {
        font-size: 20px;
        line-height: 25px;
        font-weight: bold;
        padding: 0 0 25px;

        @media(max-width: 1198px){
            padding: 20px 0 20px;
            font-size: 18px;
        }
        @media(max-width: 764px){
            font-size: 16px;
        }
    }
}

`

const Content = (props) => {
    return (
        <article>
            <LocContainer>
                {props.html}
            </LocContainer>
        </article>
    )
}

export default Content