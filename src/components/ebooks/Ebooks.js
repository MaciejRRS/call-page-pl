import React from 'react'
import { connect } from 'react-redux'
import Hero from './components/Hero'
import Content from './components/Content'
import SEO from './../../common/SEO/seo'

const Ebooks = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} />
            <Content ebooks={props.ebooks} totalEbooks={props.totalEbooks} />
        </main>
    )
}


const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[17].acf, // Ebooks
        ebooks: state.wpAcfReducer.ebooks,
        totalEbooks: state.wpAcfReducer.totalEbooks
    }
}

export default connect(mapStateToProps, {})(Ebooks)