import React from 'react'
import Hero from './components/Hero'
import { connect } from "react-redux"
import Increase from '../../common/bricks/Increase'
import Clients from './components/Clients'
import Integration from '../../common/bricks/Integration'
import IncreaseList from '../../common/bricks/Clients'
import Develope from './components/Develope'
import Video from '../../common/bricks/Video'
import About from '../../common/bricks/About'
import Awards from '../../common/bricks/Awards'
import Bussinesses from '../../common/bricks/Bussinesses'
import SEO from './../../common/SEO/seo'

const MainPage = (props) => {
    
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.first_hero} form_free_test={props.acf.form_free_test} />
            <Video acf={props.acf.second_video} />
            <Clients acf={props.acf.fifth_clients} />
            <IncreaseList acf={props.acf.increase_list} />
            <About acf={props.acf.third_about} />
            <Increase acf={props.acf.fourth_increase} />
            <Integration acf={props.acf.sixth_integration} />
            <Bussinesses acf={props.acf.bussinesses} />
            <Awards acf={props.acf.awards} />
            <Develope acf={props.acf.seventh_develope} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[1].acf // MainPage
    }
}

export default connect(mapStateToProps, {})(MainPage)