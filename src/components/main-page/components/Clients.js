import React from 'react'
import styled from 'styled-components'
import { Container, Link } from '../../../common/styles'
import ReactHtmlParser from 'react-html-parser';

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const ListItem = styled.li`
    display: flex;
    justify-content: space-evenly;
    align-items: center;

    a{  
        background-color: #F0F5FF;
        border: 1px solid #F0F5FF;
        color: #377DFF;
        box-shadow: 0px 6px 5px 3px #D7E5FF;
    }
`

const List = styled.ul`
    li:nth-child(1){
        h3{
            color: #000;
        }
        a{
            background-color: #377DFF;
            color: #ffffff;
            border: 2px solid #377DFF;
            box-shadow: 0px 3px 10px 6px #D7E5FF;
        }
    }

    li:nth-child(even){
        flex-direction: row-reverse;
    }
`

const TextPart = styled.div`
    width: 500px;
    @media(max-width: 1198px){
        max-width: 600px;
        margin: 0 auto;
        width: 100%;
    }

`

const InnerListItem = styled.li`
    padding: 15px 0 0 25px;
    position: relative;

    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 24px;
    }

    &::before{
        position: absolute;
        left: 0;
        content: "✓";
        color: #377DFF;
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 80px;
    margin: 0 auto;
    max-width: 1080px;
    @media(max-width: 1198px){
        max-width: 510px;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        padding-bottom: 60px;
        padding: 0 0 55px;
        max-width: 100%;
    }

    span {
        font-size: clamp(45px, 3.5vw, 60px);
        line-height: clamp(60px, 3.5vw, 80px);
            position: relative;
            &:after {
                position: absolute;
                content: '';
                z-index: -1;
                left: -6%;
                bottom: 3px;
                height: 4px;
                width: 110%;
                background-image: url(${props => props.background});
                background-repeat: no-repeat;
                background-size: 100% 4px;
                @media(max-width: 767px) {
                    background-size: 0;
                }
            }

            @media(max-width: 767px) {
                   color: #377DFF;
                   font-size: 28px;
                    line-height: 40px;
                }
        }
`

const ListTitle = styled.h3`
    color: #377DFF;
    padding: 0 0 10px;
    font-size: 22px;
    font-weight: bold;
    line-height: 30px;
    @media(max-width: 1198px){
        font-size: 22px;
        padding: 0 0 25px;

    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const ListText = styled.p`
    line-height: 24px;
    padding-bottom: 30px;
    color: #6E7276;
    @media(max-width: 1198px){
        line-height: 24px;
    }
`

const SubTitle = styled.h4`
font-size: 18px;
margin-bottom: 15px;
line-height: 28px;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
        margin-bottom: 10px;
    }
`

const InnerList = styled.ul`
    padding-bottom: 80px;
    @media(max-width: 1198px){
        padding-bottom: 40px;
    }
`

const ImgPart = styled.div`
    @media(max-width: 1198px){
        display: none;
    }
`

const LocLink = styled(Link)`
    transition: all.2s linear;

    &:hover {
        border-color: #377DFF!important;
        background-color: #FFFFFF!important;
        color: #000000!important;
    }

    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 28px;
    }
    @media(max-width: 640px){
        font-size: 14px;
        line-height: 24px;
        display: block;
        width: 100%;
        box-sizing: border-box;
        text-align: center;
    }
    
`

const Clients = (props) => {
    const array = Object.values(props.acf.clients_repeter)

    return (
        <Article>
            <Container>
                <Title background={props.acf.background_line}>{ReactHtmlParser(props.acf.title)}</Title>
                <List>
                    {
                        array.map(el =>
                            <ListItem>
                                <ImgPart>
                                    <img alt={el.img_alt} src={el.img2} />
                                </ImgPart>
                                <TextPart>
                                    <ListTitle>{el.title}</ListTitle>
                                    <ListText>{el.text}</ListText>
                                    <SubTitle>{el.list_title}</SubTitle>
                                    <InnerList>
                                        {
                                            el.list_items.map(innerEl =>
                                                <InnerListItem>
                                                    {innerEl.text}
                                                </InnerListItem>
                                            )
                                        }
                                    </InnerList>
                                    {
                                        el.button_type
                                            ? <LocLink to={el.button_link}>{el.button_text}</LocLink>
                                            : <LocLink as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</LocLink>
                                    }
                                </TextPart>
                            </ListItem>
                        )
                    }
                </List>
            </Container>
        </Article>
    )
}

export default Clients