import React, { useState } from 'react'
import { Container, Flex } from '../../../common/styles'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import Loupe from '../../../sprites/loupe.svg'
import Clock from '../../../sprites/clock.png'

/* eslint-disable */

const Article = styled.article`
    padding-top: 120px;

    @media(max-width: 1198px){
        padding-top: 70px;
    }

`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 6fr;
    grid-column-gap: 30px;
`

const LocFlex = styled(Flex)`
    align-items: center;
    flex-wrap: wrap;
    @media(max-width: 816px){
        flex-direction: column;
    }
`

const ItemTitle = styled.h2`
    padding: 0px 15px 105px;
    font-size: 18px;

    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
`

const LineColor = styled.img`
    display: block;
`

const Link = styled(NavLink)`
    color: #000000;
`

const Pagination = styled.div`
    padding: ${props => props.bottom ? '30px 0 0 0' : '0 0 30px 0'};
`

const SearchWrapper = styled.div`
    padding-bottom: 30px;
    width: 665px;
    position: relative;

    img{
        position: absolute;
        left: 12px;
        top: 12px;
        width: 30px;
        height: 30px;
        z-index: 2;
    }
    @media(max-width: 1198px){
        width: 333px;
        img{
            left: 14px;
            top: 14px;
            width: 24px;
            height: 24px;
        }
    }
    @media(max-width: 816px){
        width: 180px;
        img{
            left: 15px;
            top: 15px;
            width: 20px;
            height: 20px;
        }
    }

    @media(max-width: 816px){
        width: 100%;
    }
`

const Search = styled.input`
    border: 2px solid #C2C9D1;
    box-sizing: border-box;
    width: 100%;
    padding: 8px 62px;
    border-radius: 6px;
    cursor: pointer;
    position: relative;

    &:focus{
        border-color: #377DFF;
        outline: none;
    }

    @media(max-width: 1198px){
        width: 333px;
        padding: 8px 60px;

        &:input{
        font-size: 16px;
        }
    }

    @media(max-width: 816px){
        width: 180px;
        padding: 8px 5px 8px 45px;
    }

    @media(max-width: 816px){
        width: 100%;
    }
`

const Button = styled.button`
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const TypesFlex = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 15px;
`

const TypesItem = styled.p`
    padding: 7px 15px;
    border: 2px solid #377DFF;
    border-radius: 3px;
    font-size: 14px;
    line-height: 21px;
    font-weight: bold;
    margin: 0 15px 15px 0;
    cursor: pointer;
`

const InfoFlex = styled(Flex)`
    padding: 0 15px;
    position: absolute;
    bottom: 30px;
    width: calc(100% - 60px);
    color: #6E7276;
    align-items: center;
    img{
        width: 32px;
        height: 32x;
        margin-right: 5px;
    }
`

const Aside = styled.aside`
    width: 100%;
    height: 70px;
    box-shadow: 0 3px 6px 0 #00000016;
    margin-bottom: 120px;
    position: sticky;
    top: 100px;;
    background-color: #fff;
    z-index: 10;

    @media(max-width: 1198px){
        height: auto;
    }

`

const AsideContent = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0 60px;
    max-width: 1640px;
    margin: 0 auto;
    height: 100%;

    @media(max-width: 1198px){
        transition: .3s linear;
        height: ${props => props.isOpen ? `70px` : '0px'};
        transform: ${props => props.isOpen ? "translateY(0)" : 'translateY(0)'};
        opacity: ${props => props.isOpen ? "1" : '0'};
        pointer-events: ${props => props.isOpen ? "all" : 'none'};
    }
    @media(max-width: 1198px){
        height: ${props => props.isOpen ? `${props.heightMenu}px` : '0px'};
        flex-direction: column;
        justify-content: space-evenly;
    }
    @media(max-width: 500px){
        padding: 0 ;
    }
`

const AsideTitle = styled.button`
    display: none;

    @media(max-width: 1198px){
        display: block;
        margin: 0 auto;
        border: none;
        background-color: transparent;
        height: 50px;
    }
`

const AsideItem = styled.button`
    margin: 0 30px;
    border: none;
    background-color: transparent;
    cursor: pointer;
    position: relative;
    transition: .2s linear;
    font-size: 16px;
    line-height: 24px;

    &:hover {
        color: ${props => props.hoverColor};
    }

    img {
        transition: .1s linear;
        position: absolute;
        bottom: -5px;
        left: 0;
        width: 100%;
    }
`

const ItemGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 816px){
        grid-template-columns: 1fr;
    }
`

const Item = styled.div`
    max-width: 526px;
    width: 100%;
    box-sizing: border-box;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 6px;
    margin: 0 auto; 
    height: 100%;
    position: relative;

    img{
        max-width: 526px;
        width: 100%;
    }

    @media(max-width: 816px){
        max-width: 100%;

        img{
            max-width: 100%;
        }
    }
`

const Content = (props) => {
    let arrays = [[], [], []]

    for (let i = 1; i <= props.posts.length; i++) {
        if (i % 3 == 0) {
            arrays[2].push(props.posts[i - 1])
        } else if (i % 2 == 0) {
            arrays[1].push(props.posts[i - 1])
        } else {
            arrays[0].push(props.posts[i - 1])
        }
    }

    const [isOpenedMobileMenu, isOpenedMobileMenuChange] = useState(false)

    let asideHeight = 0

    props.acf.repeater.forEach(el => { asideHeight += 50 })

    const OpenMenu = () => {
        isOpenedMobileMenuChange(!isOpenedMobileMenu)
    }

    return (
        <Article>
            <Aside>
                <AsideTitle onClick={() => { OpenMenu() }}>
                    {'All Categories'}
                </AsideTitle>
                <AsideContent isOpen={isOpenedMobileMenu} heightMenu={asideHeight}>
                    {props.acf.repeater.map(el =>
                        <AsideItem hoverColor={el.underline_color_hover} onClick={() => { props.FilterByCategory(el.type) }}>{el.name}
                            <LineColor alt='underline' src={el.underline} />
                        </AsideItem>
                    )}
                </AsideContent>
            </Aside>
            <Container>
                <Grid>
                    <section>
                        <LocFlex>
                            {props.isAllPosts
                                ? <Pagination>
                                    {props.canPrev
                                        ? <Button onClick={() => { props.FirstPage() }}>{'«'}</Button>
                                        : <Button disabled onClick={() => { props.FirstPage() }}>{'«'}</Button>
                                    }
                                    {props.canPrev
                                        ? <Button onClick={() => { props.PrevPage() }}>{'‹'}</Button>
                                        : <Button disabled onClick={() => { props.PrevPage() }}>{'‹'}</Button>
                                    }
                                    <Button page={true}>{props.currentBlogPage}</Button>
                                    {props.canNext
                                        ? <Button onClick={() => { props.NextPage() }}>{'›'}</Button>
                                        : <Button disabled onClick={() => { props.NextPage() }}>{'›'}</Button>
                                    }
                                    {props.canNext
                                        ? <Button onClick={() => { props.LastPage() }}>{'»'}</Button>
                                        : <Button disabled onClick={() => { props.LastPage() }}>{'»'}</Button>
                                    }
                                </Pagination>
                                : <Pagination>
                                    <Button disabled onClick={() => { props.FirstPage() }}>{'«'}</Button>
                                    <Button disabled onClick={() => { props.PrevPage() }}>{'‹'}</Button>
                                    <Button page={true}>{props.currentBlogPage}</Button>
                                    <Button disabled onClick={() => { props.NextPage() }}>{'›'}</Button>
                                    <Button disabled onClick={() => { props.LastPage() }}>{'»'}</Button>
                                </Pagination>
                            }
                            <SearchWrapper>
                                <img alt={'loupe'} src={Loupe} />
                                {
                                    props.isAllPosts
                                        ? <Search onChange={event => props.FilterByTitle(event.target.value)} placeholder={props.acf.input_placeholder} />
                                        : <Search disabled onChange={event => props.FilterByTitle(event.target.value)} placeholder={props.acf.input_placeholder} />
                                }

                            </SearchWrapper>
                        </LocFlex>
                        {!props.isFetchingAddData
                            ? <ItemGrid>
                                {props.posts.map(el =>
                                    <Item>
                                        <Link to={'blog/' + el.slug}>
                                            <img alt={el.acf.img_alt} src={el.acf.preview.img} />
                                        </Link>
                                        <TypesFlex>
                                            {el.acf.type
                                                ? el.acf.type.map(innerEl =>
                                                    <TypesItem onClick={() => { props.FilterByCategory(innerEl.text) }}>
                                                        {innerEl.text}
                                                    </TypesItem>
                                                )
                                                : null
                                            }
                                        </TypesFlex>
                                        <Link to={'blog/' + el.slug}>
                                            <ItemTitle>{el.acf.preview.title}</ItemTitle>
                                            <InfoFlex>
                                                <div style={{ display: 'flex', 'align-items': 'center' }}>
                                                    <img style={{ width: '48px', height: '48px', 'margin-right': '10px' }} alt='post author' src={el.acf.preview.author_img} />
                                                    <div>
                                                        <p style={{ 'line-height': '24px' }}>Author</p>
                                                        <p style={{ 'line-height': '24px' }}>{el.acf.preview.author}</p>
                                                    </div>
                                                </div>
                                                <div style={{ display: 'flex', 'align-items': 'center' }}>
                                                    <img alt='clock' src={Clock} /><span>{el.acf.preview.time}{'m'}</span>
                                                </div>
                                            </InfoFlex>
                                        </Link>
                                    </Item>
                                )}
                            </ItemGrid>
                            : <div>Prosze poczekac ladowanie danych</div>
                        }
                        {props.isAllPosts
                            ? <Pagination bottom>
                                {props.canPrev
                                    ? <Button onClick={() => { props.FirstPage() }}>{'«'}</Button>
                                    : <Button disabled onClick={() => { props.FirstPage() }}>{'«'}</Button>
                                }
                                {props.canPrev
                                    ? <Button onClick={() => { props.PrevPage() }}>{'‹'}</Button>
                                    : <Button disabled onClick={() => { props.PrevPage() }}>{'‹'}</Button>
                                }
                                <Button page={true}>{props.currentBlogPage}</Button>
                                {props.canNext
                                    ? <Button onClick={() => { props.NextPage() }}>{'›'}</Button>
                                    : <Button disabled onClick={() => { props.NextPage() }}>{'›'}</Button>
                                }
                                {props.canNext
                                    ? <Button onClick={() => { props.LastPage() }}>{'»'}</Button>
                                    : <Button disabled onClick={() => { props.LastPage() }}>{'»'}</Button>
                                }
                            </Pagination>
                            : <Pagination bottom>
                                <Button disabled onClick={() => { props.FirstPage() }}>{'«'}</Button>
                                <Button disabled onClick={() => { props.PrevPage() }}>{'‹'}</Button>
                                <Button page={true}>{props.currentBlogPage}</Button>
                                <Button disabled onClick={() => { props.NextPage() }}>{'›'}</Button>
                                <Button disabled onClick={() => { props.LastPage() }}>{'»'}</Button>
                            </Pagination>
                        }
                    </section>
                </Grid>

            </Container>
        </Article >
    )
}

export default Content