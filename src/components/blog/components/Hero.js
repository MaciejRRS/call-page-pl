import React from 'react'
import { Container } from '../../../common/styles'
import styled from 'styled-components'

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    text-align: center;
    padding: 60px 0 30px;

    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }

    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        padding: 60px 0 10px;
    }
`

const SubTitle = styled.h2`
    font-size: 28px;
    line-height: 40px;
    text-align: center;
    margin: 0 auto;
    color: #3F3F3F;
    max-width: 805px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 28px;
    }

    @media(max-width: 764px){
        font-size: 18px;
        line-height: 22px;
    }
`

const Hero = (props) => {
    return (
        <article>
            <Container>
                <Title>{props.acf.title}</Title>
                <SubTitle>{props.acf.sub_title}</SubTitle>
            </Container>
        </article>
    )
}

export default Hero