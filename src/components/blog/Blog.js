import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import Hero from './components/Hero'
import Content from './components/Content'
import { blogPaginationThunk, blogInitThunk } from './../../redux/wpAcfReducer'
import SEO from './../../common/SEO/seo'

/* eslint-disable */

const Blog = (props) => {

    const FirstPage = () => {
        props.blogPaginationThunk(1)
    }
    const LastPage = () => {
        props.blogPaginationThunk(pages)
    }
    const PrevPage = () => {
        props.blogPaginationThunk(props.currentBlogPage - 1)
    }
    const NextPage = () => {
        props.blogPaginationThunk(props.currentBlogPage + 1)
    }

    const [isFilter, changeIsFilter] = useState(true)
    const [postsPrototype, changePrototype] = useState([...props.blogPosts])
    let [postsPrototypeFiltred, changePrototypeFiltred] = useState([...props.blogPosts])
    let [postsWithFilter, postsChange] = useState([...postsPrototype])
    let [postsWithOutFilter, postsWithOutFilterChange] = useState([...postsPrototype])

    useEffect(() => {
        props.blogInitThunk()
    }, [])

    useEffect(() => {
        changePrototype([...props.blogPosts])
        postsWithOutFilterChange([...props.blogPosts])
        changePrototypeFiltred([...props.blogPosts])
        postsChange([...props.blogPosts])
    }, [props])

    useEffect(() => {
        changePrototype([...props.blogPosts])
        postsChange([...props.blogPosts])
        if (isFilter) {
            postsWithOutFilterChange(postsPrototype.slice(props.currentBlogPage * 10 - 10, props.currentBlogPage * 10))
        } else {
            postsChange(postsPrototypeFiltred.slice(props.currentBlogPage * 10 - 10, props.currentBlogPage * 10))
        }
    }, [props])

    useEffect(() => {
        if (isFilter) {
            postsWithOutFilterChange(postsPrototype.slice(props.currentBlogPage * 10 - 10, props.currentBlogPage * 10))
        } else {
            postsChange(postsPrototypeFiltred.slice(props.currentBlogPage * 10 - 10, props.currentBlogPage * 10))
        }
    }, [postsPrototypeFiltred])


    const FilterByCategory = (value) => {
        if (value.toLowerCase() === 'all') {
            changeIsFilter(true)
        } else {
            changeIsFilter(false)
        }
        changePrototypeFiltred(postsPrototype.filter(el => {
            let isFiltered = false
            if (el.acf.type) {
                el.acf.type.forEach(innerEl => {
                    if (innerEl.text === value) {
                        isFiltered = true
                    } else if (value.toLowerCase() === 'all') {
                        isFiltered = true
                    }
                })
            }
            return isFiltered
        }))
        FirstPage()
    }

    const FilterByTitle = (value) => {
        changeIsFilter(!value)
        changePrototypeFiltred(postsPrototype.filter(el => {
            let text = el.acf.preview.title.toLowerCase()
            let val = value.toLowerCase()
            return text.includes(val)
        }))
        FirstPage()
    }


    let pages = Math.ceil(props.totalPosts / 10)
    let canNext = false
    let canPrev = false
    if (isFilter) {
        if (props.currentBlogPage === 1) {
            canPrev = false
        } else {
            canPrev = true
        }

        if (props.currentBlogPage === pages) {
            canNext = false
        } else {
            canNext = true
        }
    } else {
        if (props.currentBlogPage === 1) {
            canPrev = false
        } else {
            canPrev = true
        }

        if (props.currentBlogPage === Math.ceil(postsPrototypeFiltred.length / 10)) {
            canNext = false
        } else {
            canNext = true
        }
    }

    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero
                acf={props.acf.hero}
            />
            <Content
                isFilter={isFilter}
                acf={props.acf.filter}
                FilterByCategory={FilterByCategory}
                FilterByTitle={FilterByTitle}
                canPrev={canPrev}
                canNext={canNext}
                posts={isFilter ? postsWithOutFilter : postsWithFilter}
                pages={pages}
                currentBlogPage={props.currentBlogPage}
                FirstPage={FirstPage}
                LastPage={LastPage}
                PrevPage={PrevPage}
                NextPage={NextPage}
                isAllPosts={props.isAllPosts}
                isFetchingAddData={props.isFetchingAddData}
            />
        </main>
    )
}


const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[16].acf, // Blog
        blogPosts: state.wpAcfReducer.blogPosts,
        totalPosts: state.wpAcfReducer.totalPosts,
        currentBlogPage: state.wpAcfReducer.currentBlogPage,
        isAllPosts: state.wpAcfReducer.isAllPosts,
        isFetchingAddData: state.wpAcfReducer.isFetchingAddData
    }
}

export default connect(mapStateToProps, { blogPaginationThunk, blogInitThunk })(Blog)