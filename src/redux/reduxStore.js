import {applyMiddleware, combineReducers, createStore} from "redux"
import thunkMiddleware from "redux-thunk"
import wpAcfReducer from "./wpAcfReducer"

let reducers = combineReducers({
    wpAcfReducer: wpAcfReducer,
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware))

export default store